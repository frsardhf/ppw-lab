from django.urls import path, include
from django.contrib import admin
import lab_1.urls as lab_1
import lab_3.urls as lab_3


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('lab_3.urls')),
    path('story-1/', include('lab_1.urls')),

]
